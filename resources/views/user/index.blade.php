<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Caripintar') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>
    <script src="https://caripintar.maestrobyte.com/js/app.js" defer></script>
        <script src="https://caripintar.maestrobyte.com/js/date-picker-forward.js"></script>
        <script src="https://caripintar.maestrobyte.com/alpine-master/dist/alpine.js" defer></script>
    <!-- Fonts -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="https://caripintar.maestrobyte.com/build/tailwind.css">
    <link href="https://caripintar.maestrobyte.com/css/cp-basic-style.css" rel="stylesheet">

    

    <style>
    body{
    background-color: #dee9ff;
}

.registration-form{
	padding: 50px 0;
}


.registration-form form{
    background-color: #fff;
    max-width: 1000px;
    margin: auto;
    padding: 50px 70px;
    border-top-left-radius: 30px;
    border-top-right-radius: 30px;
    box-shadow: 0px 2px 10px rgba(0, 0, 0, 0.075);
    
    font-family: "Comic Sans MS", cursive, sans-serif;
    text-align : center;
    font-size: 30px;
    letter-spacing: 1px;
    word-spacing: 1.8px;
    color: #000000;
    font-weight: 700;
    text-decoration: none solid rgb(68, 68, 68);
    font-style: normal;
    font-variant: normal;
    text-transform: capitalize;
}

.event{
    padding : 10px;
    font-family: sans-serif;
    text-align : center;
    font-size: 15px;
    letter-spacing: 1px;
    word-spacing: 1.8px;
    color: #000000;
    font-weight: 700;
    text-decoration: none solid rgb(68, 68, 68);
    font-style: normal;
    font-variant: normal;
    text-transform: capitalize;
}

.welcome {
    font-family: Georgia, serif;
    font-size: 40px;
    letter-spacing: 1px;
    word-spacing: 1.8px;
    color: #000000;
    font-weight: 700;
    text-decoration: none solid rgb(68, 68, 68);
    font-style: italic;
    font-variant: small-caps;
    text-transform: capitalize;
}

.registration-form form a {
  position: relative;
  display: inline-block;
  padding: 10px 20px;
  color: 6699FF;
  font-size: 16px;
  text-decoration: none;
  text-transform: uppercase;
  overflow: hidden;
  transition: .5s;
  margin-top: 40px;
  letter-spacing: 4px
}

.registration-form a:hover {
  background: #6699FF;
  color: #fff;
  border-radius: 5px;
  box-shadow: 0 0 5px #6699FF,
              0 0 25px #6699FF,
              0 0 50px #6699FF,
              0 0 100px #6699FF;
}

.registration-form a span {
  position: absolute;
  display: block;
}

.registration-form a span:nth-child(1) {
  top: 0;
  left: -100%;
  width: 100%;
  height: 2px;
  background: linear-gradient(90deg, transparent, #03e9f4);
  animation: btn-anim1 1s linear infinite;
}

@keyframes btn-anim1 {
  0% {
    left: -100%;
  }
  50%,100% {
    left: 100%;
  }
}

.registration-form a span:nth-child(2) {
  top: -100%;
  right: 0;
  width: 2px;
  height: 100%;
  background: linear-gradient(180deg, transparent, #03e9f4);
  animation: btn-anim2 1s linear infinite;
  animation-delay: .25s
}

@keyframes btn-anim2 {
  0% {
    top: -100%;
  }
  50%,100% {
    top: 100%;
  }
}

.registration-form a span:nth-child(3) {
  bottom: 0;
  right: -100%;
  width: 100%;
  height: 2px;
  background: linear-gradient(270deg, transparent, #03e9f4);
  animation: btn-anim3 1s linear infinite;
  animation-delay: .5s
}

@keyframes btn-anim3 {
  0% {
    right: -100%;
  }
  50%,100% {
    right: 100%;
  }
}

.registration-form a span:nth-child(4) {
  bottom: -100%;
  left: 0;
  width: 2px;
  height: 100%;
  background: linear-gradient(360deg, transparent, #03e9f4);
  animation: btn-anim4 1s linear infinite;
  animation-delay: .75s
}

@keyframes btn-anim4 {
  0% {
    bottom: -100%;
  }
  50%,100% {
    bottom: 100%;
  }
}
    </style>
</head>
<body>
    <div id="app">
    <nav class="navbar navbar-dark bg-dark">
            <div class="container">
                <a class="navbar-brand" href="{{ url('/') }}">
                <img src="/img/logonew.png" alt="" width="270" height="50">
                </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                    <span class="navbar-toggler-icon"></span>
                </button>
                

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <!-- Left Side Of Navbar -->
                    <ul class="navbar-nav mr-auto">

                    </ul>
                    

                    <!-- Right Side Of Navbar -->
                    <ul class="navbar-nav ml-auto">
                        <!-- Authentication Links -->
                        @guest
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                            </li>
                            @if (Route::has('register'))
                                <li class="nav-item">
                                    <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                                </li>
                            @endif
                        @else
                            <li class="nav-item dropdown">
                                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                    {{ Auth::user()->name }} <span class="caret"></span>
                                </a>

                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                    <a class="dropdown-item" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        {{ __('Logout') }}
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                                </div>
                            </li>
                        @endguest
                    </ul>
                </div>
            </div>
        </nav>

        <main class="py-4">
            @yield('content')
        </main>
    </div>

    <div class = "welcome">
        <marquee> <h1> WELCOME USER <h1> </marquee>
    </div>


    <div class="registration-form">
        <form>
           Event

           <div class="event">
           Event Kosong
           </div>
            
           <a href="https://caripintar.maestrobyte.com/order_zoom">
                <span></span>
                <span></span>
                <span></span>
                <span></span>
               Pesan Event
            </a>   
        </form>
    </div>

    <div class="flex flex-col sm:flex-row justify-center pt-12 my-12 sm:my-4">
        <div class="flex flex-col rounded-none lg:rounded-l-lg bg-white mt-4 mx-3 sm:mx-0">
            <div class="flex-1 bg-white text-gray-600 rounded-t rounded-b-none overflow-hidden shadow">
                <div class="p-8 text-3xl font-bold text-center border-b-4">
                    Zoom PRO
                </div>
                <ul class="w-full text-center text-sm">
                <li class="border-b py-4">max 100 Participants</li>
                <li class="border-b py-4">max 99 Co-HOST</li>
                <li class="border-b py-4">HOST diberikan ke Penyewa</li>
                </ul>
            </div>
            <div class="flex-none mt-auto bg-white rounded-b rounded-t-none overflow-hidden shadow p-6">
                <div class="w-full pt-6 text-3xl text-gray-600 font-bold text-center">
                    Rp. 15.000,-
                    <span class="text-base">per jam</span>
                </div>
                <div class="flex items-center justify-center">
                    <a href="/order_zoom?g=1" class="mx-auto lg:mx-0 hover:underline gradient text-white font-bold rounded-full my-6 py-4 px-8 shadow-lg focus:outline-none focus:shadow-outline transform transition hover:scale-105 duration-300 ease-in-out">Daftar</a>
                </div>
            </div>
        </div>
        
        <div class="flex flex-col rounded-lg mt-4 sm:-mt-6 shadow-lg mx-3 sm:mx-0">
            <div class="flex-1 bg-white rounded-t rounded-b-none text-gray-600 overflow-hidden shadow">
                <div class="w-full p-8 text-3xl font-bold text-center">Large Meeting 1000</div>
                <div class="h-1 w-full gradient my-0 py-0 rounded-t"></div>
                <ul class="w-full text-center text-base font-bold">
                    <li class="border-b py-4">max 1000 Participants</li>
                    <li class="border-b py-4">max 999 Co-HOST</li>
                    <li class="border-b py-4">HOST diberikan ke Penyewa</li>
                </ul>
            </div>
            <div class="flex-none mt-auto bg-white text-gray-600 rounded-b rounded-t-none overflow-hidden shadow p-6">
                <div class="w-full pt-6 text-4xl font-bold text-center">
                    Rp. 50.000,-
                    <span class="text-base">per jam</span>
                </div>
                <div class="flex items-center justify-center">
                    <a href="/order_zoom?g=2" class="mx-auto lg:mx-0 hover:underline gradient text-white font-bold rounded-full my-6 py-4 px-8 shadow-lg focus:outline-none focus:shadow-outline transform transition hover:scale-105 duration-300 ease-in-out">Daftar</a>
                </div>
            </div>
        </div>
    </div>

    <div class= "navbar navbar-dark bg-dark">
        <div class="container">
        
      </div>
    </div>
</body>
</html>
