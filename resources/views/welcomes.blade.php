<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="pcqKAlCjo8kRa9je3Oiuccvf3TQLysDRvpnotJAP">

        <title>CariPintar</title>

        <!-- Fonts -->
        <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap">

        <!-- Styles -->
        <!-- <link rel="stylesheet" href="https://caripintar.maestrobyte.com/css/app.css"> -->
        <link rel="stylesheet" href="https://caripintar.maestrobyte.com/build/tailwind.css">
        <link href="https://caripintar.maestrobyte.com/css/css" rel="stylesheet">
        <link href="https://caripintar.maestrobyte.com/css/cp-basic-style.css" rel="stylesheet">

        <!-- Scripts -->
        <script src="https://caripintar.maestrobyte.com/js/app.js" defer></script>
        <script src="https://caripintar.maestrobyte.com/js/date-picker-forward.js"></script>
        <!-- script src="https://caripintar.maestrobyte.com/alpine-master/dist/alpine.js" defer></script -->
            </head>
    <body class="fontset-ssp antialiased">
        <div class="min-h-screen leading-normal tracking-normal text-white gradient">
            <nav id="header" class="sticky block w-full z-40 top-0 text-white gradient" x-data="{ nav: false , acount: false}">
    <div class="w-full container mx-auto flex flex-wrap items-center justify-between mt-0 py-2">
        <div class="pl-4 flex items-center">
            <a class="flex toggleColour text-white no-underline hover:no-underline font-bold text-2xl lg:text-4xl" href="/">
                <span class="pl-5 sm:pl-0 icon-caripintar mr-2"></span>
                <span class="text-md sm:text-2x1">CariPintar.com</span>
            </a>
        </div>       
                    <div class="top-0 right-0 px-6 flex">
                            <div class="sm:hidden pr-4">
                    <button @click="nav = true" class="flex items-center p-1 text-white hover:text-gray-900 focus:outline-none focus:text-blue-500">
                        <svg class="fill-current h-6 w-6" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg">
                        <path d="M0 3h20v2H0V3zm0 6h20v2H0V9zm0 6h20v2H0v-2z"></path>
                        </svg>
                    </button>
                </div>  
                <a href="/login" class="hidden sm:block inline-block text-white no-underline hover:text-gray-800 hover:text-underline">Masuk</a>

                <a href="/register" class="hidden sm:block inline-block text-white no-underline hover:text-gray-800 hover:text-underline px-4">Daftar</a>
                        </div>
            </div>
    <hr class="border-b border-gray-100 opacity-25 my-0 py-0">

    <div class="fixed bg-white text-gray-900 w-full rounded sm:hidden shadow-md" x-show.transition="nav" @click.away="nav = false">
        <ul class="text-md mx-3">
            <a href="/login"><li class="my-2 hover:text-indigo-300">Masuk</li></a>
            <hr class="border-b border-gray-900 opacity-25 my-0 py-0">
            <a href="/register"><li class="my-2 hover:text-indigo-300">Daftar</li></a>
        </ul>
    </div>

    <div class="fixed right-0 w-full sm:right-10 sm:w-56 bg-white text-gray-900 rounded shadow-md" x-show.transition="acount" @click.away="acount = false">
        <ul class="text-md mx-3">
            <a href="https://caripintar.maestrobyte.com/dashboard"><li class="my-1 py-2 hover:text-indigo-300">Member</li></a>
            <a href="https://caripintar.maestrobyte.com/profile"><li class="my-1 py-2 hover:text-indigo-300">Profile</li></a>
            <hr class="border-b border-gray-900 opacity-25 my-0 py-0">
            <form method="POST" action="https://caripintar.maestrobyte.com/logout">
            <input type="hidden" name="_token" value="pcqKAlCjo8kRa9je3Oiuccvf3TQLysDRvpnotJAP">            <button type="submit" class="focus:outline-none w-full"><li class="my-1 py-2 text-left hover:text-indigo-300">Keluar</li></button>
            </form>
        </ul>
    </div>
</nav>            
            <!-- Page Content -->
            <main>
                <div class="pt-24 md:ml-10">
        <div class="container px-3 mx-auto flex flex-wrap flex-col md:flex-row items-center -mt-14">
          <!--Left Col-->
            <div class="block md:flex md:flex-col w-full md:w-2/5 justify-center items-start text-center md:text-left">
                <p class="uppercase tracking-loose w-full">Mau buat acara online di ZOOM?</p>
                <h1 class="my-4 text-5xl font-bold leading-tight">
                Tapi tidak punya akun ZOOM?
                </h1>
                <p class="leading-normal text-2xl mb-8">
                Sewa aja jam-jam an di CariPintar.com
                </p>
                <a href="https://caripintar.maestrobyte.com/order_zoom" class="mx-auto lg:mx-0 hover:underline bg-white text-gray-800 font-bold rounded-full mt-4 lg:mt-0 py-4 px-8 shadow opacity-75 focus:outline-none focus:shadow-outline transform transition hover:scale-105 duration-300 ease-in-out">Daftar</a>
            </div>
            <!--Right Col-->
            <div class="w-full md:w-3/5 py-6 text-center">
                <img class="w-full md:w-4/5 z-50" src="https://caripintar.maestrobyte.com/image/webinar.png">
            </div>
        </div>
    </div>

    <div class="relative -mt-12 lg:-mt-14">
        <svg viewBox="0 0 1428 174" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
            <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                <g transform="translate(-2.000000, 44.000000)" fill="#FFFFFF" fill-rule="nonzero">
                    <path d="M0,0 C90.7283404,0.927527913 147.912752,27.187927 291.910178,59.9119003 C387.908462,81.7278826 543.605069,89.334785 759,82.7326078 C469.336065,156.254352 216.336065,153.6679 0,74.9732496" opacity="0.100000001"></path>
                    <path d="M100,104.708498 C277.413333,72.2345949 426.147877,52.5246657 546.203633,45.5787101 C666.259389,38.6327546 810.524845,41.7979068 979,55.0741668 C931.069965,56.122511 810.303266,74.8455141 616.699903,111.243176 C423.096539,147.640838 250.863238,145.462612 100,104.708498 Z" opacity="0.100000001"></path>
                    <path d="M1046,51.6521276 C1130.83045,29.328812 1279.08318,17.607883 1439,40.1656806 L1439,120 C1271.17211,77.9435312 1140.17211,55.1609071 1046,51.6521276 Z" id="Path-4" opacity="0.200000003"></path>
                </g>
                <g transform="translate(-4.000000, 76.000000)" fill="#FFFFFF" fill-rule="nonzero">
                    <path d="M0.457,34.035 C57.086,53.198 98.208,65.809 123.822,71.865 C181.454,85.495 234.295,90.29 272.033,93.459 C311.355,96.759 396.635,95.801 461.025,91.663 C486.76,90.01 518.727,86.372 556.926,80.752 C595.747,74.596 622.372,70.008 636.799,66.991 C663.913,61.324 712.501,49.503 727.605,46.128 C780.47,34.317 818.839,22.532 856.324,15.904 C922.689,4.169 955.676,2.522 1011.185,0.432 C1060.705,1.477 1097.39,3.129 1121.236,5.387 C1161.703,9.219 1208.621,17.821 1235.4,22.304 C1285.855,30.748 1354.351,47.432 1440.886,72.354 L1441.191,104.352 L1.121,104.031 L0.457,34.035 Z"></path>
                </g>
            </g>
        </svg>
    </div>

    <section class="bg-white border-b py-8">
        <!-- Head -->
        <div class="container max-w-5xl mx-auto m-8">
            <h1 class="w-full my-2 text-5xl font-bold leading-tight text-center text-gray-800">
                Sewa ZOOM Meeting
            </h1>
            <div class="w-full mb-4">
                <div class="h-1 mx-auto gradient w-64 opacity-25 my-0 py-0 rounded-t"></div>
            </div>

        <!-- Content -->
            <div class="flex flex-wrap">
                <div class="w-5/6 sm:w-1/2 p-6 mx-auto sm:mx-0">
                    <h3 class="text-3xl text-gray-800 font-bold leading-none mb-3">
                    Sewa ZOOM per Event <br>(per jam hanya Rp. 15.000,-)
                    </h3>
                    <p class="text-gray-600 mb-8">
                    Tidak setiap hari buat acara ZOOM? Buat apa sewa bulanan? Tidak masalah, sewa aja per event, hanya Rp. 15.000,- per jam nya. 
                    </p>
                    <div class="flex items-center justify-start">
                        <a href="/order_zoom?g=1" class="mx-auto lg:mx-0 hover:underline gradient text-white font-bold rounded-full my-6 py-4 px-8 shadow-lg focus:outline-none focus:shadow-outline transform transition hover:scale-105 duration-300 ease-in-out">Daftar</a>
                    </div>
                </div>

                <div class="w-full sm:w-1/2 p-6">
                    <img class="w-full md:w-4/5 z-50" src="https://caripintar.maestrobyte.com/image/zoom_perjam.png">
                </div>
            </div>

            <div class="flex flex-wrap flex-col-reverse sm:flex-row">
                <div class="w-full sm:w-1/2 p-6 mt-6">
                    <img class="w-full md:w-4/5 z-50" src="https://caripintar.maestrobyte.com/image/zoom_webinar.png">
                </div>
                <div class="w-5/6 sm:w-1/2 p-6 mt-6 mx-auto sm:mx-0">
                    <div class="align-middle">
                        <h3 class="text-3xl text-gray-800 font-bold leading-none mb-3">
                        ZOOM Meeting 1.000 participant? <br>
                        (per jam hanya Rp. 50.000,-)
                        </h3>
                        <p class="text-gray-600 mb-8">
                        Punya ZOOM Meeting tapi cuma 100 participant saja? Perlu ZOOM Meeting dengan kapasitas hingga 1.000 participant? Solusi nya adalah sewa per event, hanya Rp. 50.000,- per jam nya.
                        </p>
                        <div class="flex items-center justify-start">
                            <a href="/order_zoom?g=2" class="mx-auto lg:mx-0 hover:underline gradient text-white font-bold rounded-full my-6 py-4 px-8 shadow-lg focus:outline-none focus:shadow-outline transform transition hover:scale-105 duration-300 ease-in-out">Daftar</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="bg-white border-b py-8">

        <div class="container mx-auto flex flex-wrap pt-4 pb-12">
            <!-- Head -->
            <h1 class="w-full my-2 text-5xl font-bold leading-tight text-center text-gray-800">
            Pelanggan Kami
            </h1>
            <div class="w-full mb-4">
                <div class="h-1 mx-auto gradient w-64 opacity-25 my-0 py-0 rounded-t"></div>
            </div>

            <!-- Content -->
            <div class="w-full md:w-1/3 p-6 flex flex-col flex-grow flex-shrink">
                <div class="flex-1 bg-white rounded-t rounded-b-none overflow-hidden shadow">
                <a href="https://caripintar.maestrobyte.com/order_zoom" class="flex flex-wrap no-underline hover:no-underline">
                    <p class="w-full text-gray-600 text-xs md:text-sm px-6">
                    ZOOM Meeting 1000 Participant
                    </p>
                    <div class="w-full font-bold text-xl text-gray-800 px-6">
                    Jakarta Aquarium &amp; Safari
                    </div>
                    <p class="text-gray-800 text-base px-6 mb-5">
                    Untuk mengadakan Virtual Tour bagi anak-anak sekolah, kami menggunakan Room dari CariPintar.com dan pelayanan nya sangat baik
                    </p>
                </a>
                </div>
            </div>
            <div class="w-full md:w-1/3 p-6 flex flex-col flex-grow flex-shrink">
                <div class="flex-1 bg-white rounded-t rounded-b-none overflow-hidden shadow">
                    <a href="https://caripintar.maestrobyte.com/order_zoom" class="flex flex-wrap no-underline hover:no-underline">
                        <p class="w-full text-gray-600 text-xs md:text-sm px-6">
                        Paket Webinar Basic
                        </p>
                        <div class="w-full font-bold text-xl text-gray-800 px-6">
                        INTENTION (Boutique Brand and Marketing Studio)
                        </div>
                        <p class="text-gray-800 text-base px-6 mb-5">
                        Kami bekerja sama dengan CariPintar.com untuk menyelenggarakan event online dan ami merasa sangat puas. Tim CariPintar.com memberikan arahan, solusi serta pelaksanaan yang baik. Terima kasih!
                        </p>
                    </a>
                </div>
            </div>
            <div class="w-full md:w-1/3 p-6 flex flex-col flex-grow flex-shrink">
                <div class="flex-1 bg-white rounded-t rounded-b-none overflow-hidden shadow">
                <a href="https://caripintar.maestrobyte.com/order_zoom" class="flex flex-wrap no-underline hover:no-underline">
                    <p class="w-full text-gray-600 text-xs md:text-sm px-6">
                    Closed Gate System with 1000 Participant
                    </p>
                    <div class="w-full font-bold text-xl text-gray-800 px-6">
                    Pemprof Jawa Tengah
                    </div>
                    <p class="text-gray-800 text-base px-6 mb-5">
                    CariPintar menjalankan sistem closed gate bagi peserta event, sehingga hanya peserta yang mendapatkan Link yang dapat join ke Zoom Room
                    </p>
                </a>
                </div>
            </div>

        </div>
    </section>

    <section class="bg-gray-100 py-8">
        <div class="container mx-auto px-2 pt-4 pb-12 text-gray-800">
            <!-- Head -->
            <h1 class="w-full my-2 text-5xl font-bold leading-tight text-center text-gray-800">
                Harga
            </h1>
            <div class="w-full mb-4">
                <div class="h-1 mx-auto gradient w-64 opacity-25 my-0 py-0 rounded-t"></div>
            </div>

            <!-- Content -->
            <div class="flex flex-col sm:flex-row justify-center pt-12 my-12 sm:my-4">
                <div class="flex flex-col w-5/6 lg:w-1/4 mx-auto lg:mx-0 rounded-none lg:rounded-l-lg bg-white mt-4">
                    <div class="flex-1 bg-white text-gray-600 rounded-t rounded-b-none overflow-hidden shadow">
                        <div class="p-8 text-3xl font-bold text-center border-b-4">
                            Zoom PRO
                        </div>
                        <ul class="w-full text-center text-sm">
                        <li class="border-b py-4">max 100 Participants</li>
                        <li class="border-b py-4">max 99 Co-HOST</li>
                        <li class="border-b py-4">HOST diberikan ke Penyewa</li>
                        </ul>
                    </div>
                    <div class="flex-none mt-auto bg-white rounded-b rounded-t-none overflow-hidden shadow p-6">
                        <div class="w-full pt-6 text-3xl text-gray-600 font-bold text-center">
                            Rp. 15.000,-
                            <span class="text-base">per jam</span>
                        </div>
                        <div class="flex items-center justify-center">
                            <a href="/order_zoom?g=1" class="mx-auto lg:mx-0 hover:underline gradient text-white font-bold rounded-full my-6 py-4 px-8 shadow-lg focus:outline-none focus:shadow-outline transform transition hover:scale-105 duration-300 ease-in-out">Daftar</a>
                        </div>
                    </div>
                </div>
                
                <div class="flex flex-col w-5/6 lg:w-1/3 mx-auto lg:mx-0 rounded-lg bg-white mt-4 sm:-mt-6 shadow-lg">
                    <div class="flex-1 bg-white rounded-t rounded-b-none overflow-hidden shadow">
                        <div class="w-full p-8 text-3xl font-bold text-center">Large Meeting 1000</div>
                        <div class="h-1 w-full gradient my-0 py-0 rounded-t"></div>
                        <ul class="w-full text-center text-base font-bold">
                            <li class="border-b py-4">max 1000 Participants</li>
                            <li class="border-b py-4">max 999 Co-HOST</li>
                            <li class="border-b py-4">HOST diberikan ke Penyewa</li>
                        </ul>
                    </div>
                    <div class="flex-none mt-auto bg-white rounded-b rounded-t-none overflow-hidden shadow p-6">
                        <div class="w-full pt-6 text-4xl font-bold text-center">
                            Rp. 50.000,-
                            <span class="text-base">per jam</span>
                        </div>
                        <div class="flex items-center justify-center">
                            <a href="/order_zoom?g=2" class="mx-auto lg:mx-0 hover:underline gradient text-white font-bold rounded-full my-6 py-4 px-8 shadow-lg focus:outline-none focus:shadow-outline transform transition hover:scale-105 duration-300 ease-in-out">Daftar</a>
                        </div>
                    </div>
                </div>

                <div class="flex flex-col w-5/6 lg:w-1/4 mx-auto lg:mx-0 rounded-none lg:rounded-l-lg bg-white mt-4">
                    <div class="flex-1 bg-white text-gray-600 rounded-t rounded-b-none overflow-hidden shadow">
                        <div class="p-8 text-3xl font-bold text-center border-b-4">
                            Paket Webinar Basic<br>
                            (Large Meetin 1000 + Full Team)
                        </div>
                        <ul class="w-full text-center text-sm">
                        <li class="border-b py-4">Bantuan Penyusunan Acara</li>
                        <li class="border-b py-4">Termasuk Tim Multimedia</li>
                        <li class="border-b py-4">Termasuk Cyber Army</li>
                        <li class="border-b py-4">1x Rapat Koordinasi</li>
                        <li class="border-b py-4">1x Gladi Bersih</li>
                        </ul>
                    </div>
                    <div class="flex-none mt-auto bg-white rounded-b rounded-t-none overflow-hidden shadow p-6">
                        <div class="w-full pt-6 text-3xl text-gray-600 font-bold text-center">
                        Rp. 1.750K
                        <span class="text-base">per event</span>
                        </div>
                        <div class="flex items-center justify-center">
                            <a href="https://caripintar.maestrobyte.com/order_zoom" class="mx-auto lg:mx-0 hover:underline gradient text-white font-bold rounded-full my-6 py-4 px-8 shadow-lg focus:outline-none focus:shadow-outline transform transition hover:scale-105 duration-300 ease-in-out">Daftar</a>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </section>
            </main>
            <!-- Change the colour #f8fafc to match the previous section colour -->
<svg class="wave-top" viewBox="0 0 1439 147" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
        <g transform="translate(-1.000000, -14.000000)" fill-rule="nonzero">
        <g class="wave" fill="#f8fafc">
            <path d="M1440,84 C1383.555,64.3 1342.555,51.3 1317,45 C1259.5,30.824 1206.707,25.526 1169,22 C1129.711,18.326 1044.426,18.475 980,22 C954.25,23.409 922.25,26.742 884,32 C845.122,37.787 818.455,42.121 804,45 C776.833,50.41 728.136,61.77 713,65 C660.023,76.309 621.544,87.729 584,94 C517.525,105.104 484.525,106.438 429,108 C379.49,106.484 342.823,104.484 319,102 C278.571,97.783 231.737,88.736 205,84 C154.629,75.076 86.296,57.743 0,32 L0,0 L1440,0 L1440,84 Z"></path>
        </g>
        <g transform="translate(1.000000, 15.000000)" fill="#FFFFFF">
            <g transform="translate(719.500000, 68.500000) rotate(-180.000000) translate(-719.500000, -68.500000) ">
            <path d="M0,0 C90.7283404,0.927527913 147.912752,27.187927 291.910178,59.9119003 C387.908462,81.7278826 543.605069,89.334785 759,82.7326078 C469.336065,156.254352 216.336065,153.6679 0,74.9732496" opacity="0.100000001"></path>
            <path d="M100,104.708498 C277.413333,72.2345949 426.147877,52.5246657 546.203633,45.5787101 C666.259389,38.6327546 810.524845,41.7979068 979,55.0741668 C931.069965,56.122511 810.303266,74.8455141 616.699903,111.243176 C423.096539,147.640838 250.863238,145.462612 100,104.708498 Z" opacity="0.100000001"></path>
            <path d="M1046,51.6521276 C1130.83045,29.328812 1279.08318,17.607883 1439,40.1656806 L1439,120 C1271.17211,77.9435312 1140.17211,55.1609071 1046,51.6521276 Z" opacity="0.200000003"></path>
            </g>
        </g>
        </g>
    </g>
</svg>

<section class="container mx-auto text-center py-6 pb-20">
    <h1 class="w-full my-2 text-5xl font-bold leading-tight text-center text-white">
        Ingin Info Lebih?
    </h1>
    <div class="w-full mb-4">
        <div class="h-1 mx-auto bg-white w-1/6 opacity-25 my-0 py-0 rounded-t"></div>
    </div>
    <h3 class="my-4 text-3xl leading-tight">
        Hubungi kami melalui Whatsapp
    </h3>

    <a href="http://wa.me/6287821520580" class="mx-auto lg:mx-0 hover:underline bg-white text-gray-800 font-bold rounded-full my-6 py-4 px-8 shadow-lg focus:outline-none focus:shadow-outline transform transition hover:scale-105 duration-300 ease-in-out">
        WA
    </a>
</section>        </div>
    </body>
</html>