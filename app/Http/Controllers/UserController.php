<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User as model;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    public function index(){
    	$users = model::all();

    	return view('user.table',compact('users'));
    }


}
